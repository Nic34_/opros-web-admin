import 'es6-promise/auto'
import 'babel-polyfill'

import Vue from 'vue'
import App from './App.vue'
import router from '../core/router'
import store from '../core/store'
import './registerServiceWorker'
import CKEditor from '@ckeditor/ckeditor5-vue';
import VueNestable from 'vue-nestable'

Vue.use( CKEditor );
Vue.use( VueNestable );

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    if (localStorage) this.$store.commit('Auth/Restore');
  },
}).$mount('#app')
