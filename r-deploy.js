var FtpDeploy = require('ftp-deploy');
var ftpDeploy = new FtpDeploy();

var config = {
    user: "j693917_repair",                   // NOTE that this was username in 1.x
    password: "asdfhweiryujsd",           // optional, prompted if none given
    host: "j693917.myjino.ru",
    port: 21,
    localRoot: __dirname + '/dist',
    remoteRoot: '/',
     include: ['*', '**/*'],      // this would upload everything except dot files
    deleteRemote: false,              // delete ALL existing files at destination before uploading, if true
    forcePasv: true                 // Passive mode is forced (EPSV command is not sent)
}

// use with promises
ftpDeploy.deploy(config)
    .then(res => console.log('finished deploy'))
    .catch(err => console.log(err))
